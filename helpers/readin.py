import yaml
from pathlib import Path


def fastq(project_path):
    
    config_folder = project_path / '_config'
    input_fastq_stream = open(Path(config_folder / 'input_fastq.yml'), 'r')
    input_fastq = yaml.load(input_fastq_stream)
    
    return input_fastq


def deseq2_groups(project_path):
    
    config_folder = project_path / '_config'
    deseq2_group_stream = open(Path(config_folder / 'deseq2_group.yml'), 'r')
    deseq2_group_data = yaml.load(deseq2_group_stream)
    
    return deseq2_group_data


def deseq2_compare(project_path):

    config_folder = project_path / '_config'
    deseq2_compare_stream = open(Path(config_folder / 'deseq2_compare.yml'), 'r')
    deseq2_compare_data = yaml.load(deseq2_compare_stream)
    
    return deseq2_compare_data