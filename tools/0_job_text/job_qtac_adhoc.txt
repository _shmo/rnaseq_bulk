
cd /projects/b1036/shmo/rnaseq
module load anaconda3/2018.12
python ./helpers/htseq_merge.py $smpl $htseq_path_str
python ./helpers/unlink.py $smpl $trim_path_str $star_path_str