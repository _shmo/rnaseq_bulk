# import std libraries
import pandas as pd
import numpy as np
import argparse
import yaml
import sys
from pathlib import Path
# import scripts
pipeline_path = 
sys.path.insert(1, pipeline_path)
from helpers import prep


parser = argparse.ArgumentParser(description='HTseq files to merge')
parser.add_argument('smpl', type=str, help='Sample to combine HTseq')
parser.add_argument('htseq_path_str', type=str, help='Path to htseq path')
args = parser.parse_args()


def merge(smpl, htseq_path_str):

    htseq_path = Path(htseq_path_str)
    pair_all_path = list(htseq_path.glob('{}*paired_all*'.format(smpl)))[0]
    unpair_R1_path = list(htseq_path.glob('{}*unpaired_R1*'.format(smpl)))[0]
    unpair_R2_path = list(htseq_path.glob('{}*unpaired_R2*'.format(smpl)))[0]
    pair_all = pd.read_csv(pair_all_path, sep='\t', header=1 ,names=['gene','count'])[0:-5]
    unpair_R1 = pd.read_csv(unpair_R1_path, sep='\t', header=1 ,names=['gene','count'])[0:-5]
    unpair_R2 = pd.read_csv(unpair_R2_path, sep='\t', header=1 ,names=['gene','count'])[0:-5]
    # pair_cut is an option that can be revisited later if RNA seq quality looks bad
    # pair_cut = pd.read_csv(htseq_smpl_dir[1], sep='\t', header=1 ,names=['gene','count'])[0:-5] 

    unpair_all = unpair_R1.merge(unpair_R2, how='left', on=['gene'], suffixes = ('R1','R2'))
    unpair_all['count'] = unpair_all['countR1'] + unpair_all['countR2']
    unpair_all = unpair_all[['gene','count']]

    htseq_merge = pair_all.merge(unpair_all, how='left', on=['gene'], suffixes = ('pair','unpair'))
    htseq_merge['count'] = htseq_merge['countpair'] + htseq_merge['countunpair']
    htseq_merge = htseq_merge[['gene','count']]

    txt_name = '{}.merge.count'.format(smpl) 

    output_path = htseq_path / txt_name
    htseq_merge.to_csv(Path(output_path), sep='\t', index=None, header=False)
    
    
merge(args.smpl, args.htseq_path_str)