#!/usr/bin/perl

use strict;
use warnings;

my $usage = 'annotate_deseq2.pl <in><annotation><out>
';
my ($in,$annotation,$out) = @ARGV;
die $usage unless @ARGV==3;
open(IN,$in) || die "Could not open $in\n";
open(ANN,$annotation) || die "Could not open $annotation\n";
open(OUT,">>$out") || die "Could not open $out\n";
my %annotation_info = ();
while(<ANN>)
 {
   chomp;
   next if /^GeneSymbol/;
   my ($symbol,$title,$loc,$strand)    = split(/\t/);
   $annotation_info{$symbol}{'loc'}    = $loc;
   $annotation_info{$symbol}{'title'}   = $title;
   $annotation_info{$symbol}{'strand'} = $strand;
 }
close ANN;

my $annotated_data;
my @header;
my $header;
my @deseq2;
my $id;
my $location;
while(<IN>)
{
 chomp;
 if (/^,/)
  {
    @header = split(/,/);
    shift @header;
    $header = join("\t","GeneSymbol","Description","Location","Strand",@header);
    print OUT "$header\n";
  }
 @deseq2 = split(/,/);
 #print "@deseq2\n";
 $id     = shift @deseq2;
 #print "$id\n";
 if(exists($annotation_info{$id}{'loc'}))
  {
    print "foo\n";
    $location       = $annotation_info{$id}{'loc'};
    $annotated_data = join("\t",$id,$annotation_info{$id}{'title'},$annotation_info{$id}{'strand'},@deseq2);
    print OUT "$annotated_data\n";
  }
}
close IN;
close OUT;

