import numpy as np
import pandas as pd
import yaml
import shutil
import datetime
from pathlib import Path
# reference User version of pipeline
pipeline_path = 


class project_dir:

    @staticmethod
    def launch():
        
        # read local project.yml file and create project directory
        project_yaml_stream = open(Path('./_project.yml'), 'r')
        project_yaml = yaml.load(project_yaml_stream)
        project_name = project_yaml['project']['name']
        project_path = Path(project_yaml['project']['path']) / '__rnaseq__{}'.format(project_name)
        project_path.mkdir(exist_ok=True)
        
        # copy _project.yml to the config file
        transfer_file = Path(pipeline_path) / '_project.yml' #-------------------
        dup_path = Path(pipeline_path) / 'config/'
        shutil.copy2(transfer_file, dup_path)
        
        # copy over the config folder to the project dir
        transfer_config = Path(pipeline_path) / 'config/'
        dup_path = project_path / '_config'
        try:
            shutil.copytree(transfer_config, dup_path)
        except FileExistsError:
            print('This configuration directory had already been created.')
            
        # copy over create_job.py script to project dir
        transfer_file = Path(pipeline_path) / 'helpers/create_jobs.py'
        shutil.copy2(transfer_file, project_path)
        
        # create output folders in directory
        fastqc_path = project_path / '1_fastqc'
        fastqc_path.mkdir(exist_ok=True)
        
        trim_path = project_path / '2_trim'
        trim_path.mkdir(exist_ok=True)
        
        star_path = project_path / '3_star'
        star_path.mkdir(exist_ok=True)
        
        htseq_path = project_path / '4_htseq'
        htseq_path.mkdir(exist_ok=True)
        
        deseq2_path = project_path / '5_deseq2'
        deseq2_path.mkdir(exist_ok=True)
        
        gsva_path = project_path / '6_gsva'
        gsva_path.mkdir(exist_ok=True)
        
    
    @staticmethod
    def acquire_info():
        
        # read local project.yml file and create project directory
        # ensure that correct project information is in project.yml when running create_job.py
        project_yaml_stream = open(Path('./_config/_project.yml'), 'r')
        project_yaml = yaml.load(project_yaml_stream)
        project_name = project_yaml['project']['name']
        project_path = Path(project_yaml['project']['path']) / '__rnaseq__{}'.format(project_name)
        
        # acquire name of output paths
        fastqc_path = project_path / '1_fastqc'
        trim_path = project_path / '2_trim'
        star_path = project_path / '3_star'
        htseq_path = project_path / '4_htseq'
        deseq2_path = project_path / '5_deseq2'
        gsva_path = project_path / '6_gsva'
        
        return project_path, fastqc_path, trim_path, star_path, htseq_path, deseq2_path, gsva_path